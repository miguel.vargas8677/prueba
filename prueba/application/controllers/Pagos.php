<?php
class Pagos extends CI_Controller{
public function __construct(){
   parent:: __construct();
   $this->load->model("pago");

}

public function index(){
$data["listadoPagos"]=$this->pago->consultarTodos();
$this->load->view("header");
$this->load->view("pagos/index",$data);
$this->load->view("footer");

}

public function nuevo(){

$this->load->view("header");
$this->load->view("pagos/nuevo");
$this->load->view("footer");

}

public function editar($id_pag){

        $data["pago"]=$this->pago->consultarPorId($id_pag);
        $this->load->view("header");
        $this->load->view("pagos/editar",$data);
        $this->load->view("footer");
      }




              public function procesarActualizacion(){
                  $id_pag=$this->input->post("id_pag");
                  $datosPagoEditado=array(
                      "identificacion_pag"=>$this->input->post("identificacion_pag"),
                      "titulo_pag"=>$this->input->post("titulo_pag"),
                      "interes_pag"=>$this->input->post("interes_pag"),
                      "recargo_pag"=>$this->input->post("recargo_pag"),

                      "estado_pag"=>$this->input->post("estado_pag"),

                  );
                  if($this->pago->actualizar($id_pag,$datosPagoEditado)){
                      //echo "INSERCION EXITOSA";
                      redirect("Pagos/index");
                  }else{
                      echo "ERROR AL ACTUALIZAR";
                  }
              }



public function guardarPago(){
  $datosNuevoPago=array(
  "identificacion_pag"=>$this->input->post("identificacion_pag"),
  "titulo_pag"=>$this->input->post("titulo_pag"),
  "interes_pag"=>$this->input->post("interes_pag"),
    "recargo_pag"=>$this->input->post("recargo_pag"),
  "estado_pag"=>$this->input->post("estado_pag")
  );
  //Logica de Negocio necesaria para subir la FOTOGRAFIA del cliente
            $this->load->library("upload");//carga de la libreria de subida de archivos
            $nombreTemporal="foto_pago_".time()."_".rand(1,5000);
            $config["file_name"]=$nombreTemporal;
            $config["upload_path"]=APPPATH.'../uploads/pagos/';
            $config["allowed_types"]="jpeg|jpg|png";
            $config["max_size"]=2*1024; //2MB
            $this->upload->initialize($config);
            //codigo para subir el archivo y guardar el nombre en la BDD

            if($this->upload->do_upload("foto_pag")){
                        $dataSubida=$this->upload->data();
                        $datosNuevoPago["foto_pag"]=$dataSubida["file_name"];
                      }

                      if($this->pago->insertar($datosNuevoPago)){
                          $this->session->set_flashdata("confirmacion","Cliente insertado exitosamente.");
                      }else{
                         $this->session->set_flashdata("error","Error al procesar, intente nuevamente.");
                      }
                      redirect("pagos/index");
                  }

                  public function procesarEliminacion($id_pag){
                    if ($this->session->userdata("c0nectadoUTC")->perfil_usu=="ADMINISTRADOR") {
                      if($this->pago->eliminar($id_pago)){
                          redirect("pagos/index");
                      }else{
                          echo "ERROR AL ELIMINAR";
                      }
                    } else {
                      redirect("seguridades/formularioLogin");
                    }
                  }

              }//cierre de la clase
          ?>
