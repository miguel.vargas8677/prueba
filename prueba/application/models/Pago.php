<?php
class Pago extends CI_Model{
public function  __construct(){

parent::__construct();

}
public function insertar($datos){
return $this->db->insert("pago",$datos);
}
//funcion para actualizar
      public function actualizar($id_pag,$datos){
        $this->db->where("id_pag",$id_pag);
        return $this->db->update("pago",$datos);
      }
      //funcion para sacar el detalle de un cliente
          public function consultarPorId($id_pag){
            $this->db->where("id_pag",$id_pag);

            $pago=$this->db->get("pago");
            if($pago->num_rows()>0){
              return $pago->row();//cuando SI hay clientes
            }else{
              return false;//cuando NO hay clientes
            }
          }
            //funcion para consultar todos lo clientes

public function consultarTodos(){
$listadoPagos=$this->db->get('pago');
if ($listadoPagos->num_rows()>0) {
  //cuando si ahy clientes
    return $listadoPagos;
}else{
  //cuando no ahi clientes
    return false;


    }
 }


 public function eliminar($id_pag){
   $this->db->where("id_pag",$id_pag);
   return $this->db->delete("pago");

 }
}

?>
