<br>
<div class="container">
<div class="row">

<div class="col-md-12">

<form action="<?php echo site_url(); ?>/pagos/guardarPago"
  method="post"
  id="frm_nuevo_pago"
  enctype="multipart/form-data"
  >


    <br>
    <br>
    <label for="">IDENTIFICACIÓN</label>
    <input class="form-control"  type="number" name="identificacion_pag" id="identificacion_pag" placeholder="Por favor Ingrese la Identificacion">
    <br>
    <br>
    <label for="">TITULO</label>
    <input class="form-control"  type="text" name="titulo_pag" id="titulo_pag" placeholder="Por favor Ingrese el apellido">
    <br>
    <br>
    <label for="">INTERES</label>
    <input class="form-control"  type="text" name="interes_pag" id="interes_pag" placeholder="Por favor Ingrese el nombre">
    <br>
    <br>
    <label for="">RECARGO</label>
    <input class="form-control"  type="text" name="recargo_pag" id="recargo_pag" placeholder="Por favor Ingrese el telefono">

    <label for="">ESTADO</label>
    <select class="form-control" name="estado_pag" id="estado_pag">
        <option value="">--Seleccione--</option>
        <option value="ACTIVO">ACTIVO</option>
        <option value="INACTIVO">INACTIVO</option>
    </select>
    <br>
    <br>
    <label for="">FOTOGRAFIA</label>
    <input type="file" name="foto_pag"
    accept="image/*"
    id="foto_pag"  value="" >
    <br>
    <br>
    <button type="submit" name="button" class="btn btn-primary">
      GUARDAR
    </button>
    &nbsp;&nbsp;&nbsp;
    <a href="<?php echo site_url(); ?>/pagos/index"
      class="btn btn-warning">
      <i class="fa fa-times"> </i>
      CANCELAR
    </a>

</form>
</div>
</div>
</div>
<script type="text/javascript">
    $("#frm_nuevo_pago").validate({
      rules:{

        },
        identificacion_pag:{
          required:true,
          minlength:10,
          maxlength:10,
          digits:true
        },
        titulo_pag:{
          letras:true,
          required:true
        }
      },
      messages:{

        },
        identificacion_pag:{
          required:"Por favor ingrese el número de cédula",
          minlength:"La cédula debe tener mínimo 10 digitos",
          maxlength:"La cédula debe tener máximo 10 digitos",
          digits:"La cédula solo acepta números"
        },
        titulo_pag:{
          letras:"Apellido Incorrecto",
          required:"Por favor ingrese el apellido"
        }
      }
    });
</script>

<script type="text/javascript">
      $("#foto_pag").fileinput({
        allowedFileExtensions:["jpeg","jpg","png"],
        dropZoneEnabled:true
      });
</script>









<!--  -->
