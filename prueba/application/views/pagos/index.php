<br>
<center>
  <h2>LISTADO DE PAGOS</h2>
</center>
<hr>
<br>
<center>
    <a href="<?php echo site_url(); ?>/pagos/nuevo" class="btn btn-primary">
      <i class="fa fa-plus-circle"></i>
      Agregar Nuevo
    </a>
</center>

<?php if ($listadoPagos): ?>
  <table class="table" id="tbl-pagos">
        <thead>
            <tr>
              <th class="text-center">ID</th>
              <th class="text-center">FOTO</th>
              <th class="text-center">IDENTIFICACIÓN</th>
              <th class="text-center">TITULO</th>
              <th class="text-center">INTERES</th>
                <th class="text-center">RECARGO</th>
              <th class="text-center">ESTADO</th>
              <th class="text-center">OPCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($listadoPagos->result()
            as $filaTemporal): ?>
                  <tr>
                    <td class="text-center">
                      <?php echo $filaTemporal->id_pag; ?>
                    </td>
                    <td class="text-center">
                      <?php if ($filaTemporal->foto_pago!=""): ?>
                        <img
                        src="<?php echo base_url(); ?>/uploads/pagos/<?php echo $filaTemporal->foto_pag; ?>"
                        height="80px"
                        width="100px"
                        alt="">
                      <?php else: ?>
                        N/A
                      <?php endif; ?>
                    </td>
                    <td class="text-center">
                      <?php echo $filaTemporal->identificacion_pag; ?>
                    </td>
                    <td class="text-center">
                      <?php echo $filaTemporal->titulo_pag; ?>
                    </td>
                    <td class="text-center">
                      <?php echo $filaTemporal->interes_pag; ?>
                    </td>
                    <td class="text-center">
                      <?php echo $filaTemporal->recargo_pag; ?>
                    </td>

                    <td class="text-center">
                      <?php if ($filaTemporal->estado_pag=="ACTIVO"): ?>
                        <div class="alert alert-success">
                          <?php echo $filaTemporal->estado_pag; ?>
                        </div>
                      <?php else: ?>
                        <div class="alert alert-danger">
                          <?php echo $filaTemporal->estado_pag; ?>
                        </div>
                      <?php endif; ?>
                    </td>

                    <td class="text-center">
                        <a href="<?php echo site_url(); ?>/pagos/editar/<?php echo $filaTemporal->id_pag; ?>" class="btn btn-warning">
                          <i class="fa fa-pen"></i>
                        </a>

                        <?php if ($this->session->userdata("c0nectadoUTC")->perfil_usu=="ADMINISTRADOR"): ?>
                          <a href="javascript:void(0)"
                           onclick="confirmarEliminacion('<?php echo $filaTemporal->id_pag; ?>');"
                           class="btn btn-danger">
                            <i class="fa fa-trash"></i>
                          </a>
                        <?php endif; ?>
                    </td>
                  </tr>
            <?php endforeach; ?>
        </tbody>
  </table>
<?php else: ?>
<div class="alert alert-danger">
    <h3>No se encontraron clientes registrados</h3>
</div>
<?php endif; ?>

<script type="text/javascript">
    function confirmarEliminacion(id_pag){
          iziToast.question({
              timeout: 20000,
              close: false,
              overlay: true,
              displayMode: 'once',
              id: 'question',
              zindex: 999,
              title: 'CONFIRMACIÓN',
              message: '¿Esta seguro de eliminar el cliente de forma pernante?',
              position: 'center',
              buttons: [
                  ['<button><b>SI</b></button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                      window.location.href=
                      "<?php echo site_url(); ?>/pagos/procesarEliminacion/"+id_pag;

                  }, true],
                  ['<button>NO</button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                  }],
              ]
          });
    }
</script>

<script type="text/javascript">
    //Deber incorporar botones de EXPORTACION
    $("#tbl-clientes").DataTable();
</script>










<!--  -->
